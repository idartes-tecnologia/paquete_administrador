<?php

namespace idartes\usuario\Repository;
use idartes\usuario\User;
use idartes\usuario\ParametroDetalles as ParametroDetalle;
use Illuminate\Support\Facades\DB;

class ParametroDetalleRepository
{

    static function tipoUsuario($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 1:
                $estado = 'SUPERVISOR';
            break;
            case 2:
                $estado = 'JEFE AREA';
            break;
            case 3:
                $estado = 'APOYO';
            break;
            default:
                $estado = 0;
            break;
        }

        return $estado;
    }


    static function tipoEstado($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 0:
                $estado = 'NO';
            break;
            case 1:
                $estado = 'SI';
            break;

            default:
                $estado = 0;
            break;
        }

        return $estado;
    }

    static function obtenerAnios(){
        $anios=array();
        for ($i=date("Y")-10; $i <= date("Y") ; $i++) { 
            $anios[$i]=$i;
        }
        return $anios;        
    }

    static function obtenerUsuarios(){
        return User::where('vc_estado','=',1)->get()->pluck('full_name', 'id')->toArray();
    }

    static function obtenerParametro($parametro){
        return ParametroDetalle::where('i_fk_id_parametro',$parametro) 
                ->where('i_estado',1)->pluck('vc_parametro_detalle', 'i_pk_id')->toArray();
    }

    static function obtenerTablasConTriggers(){
        $tablas = DB::select('SELECT  DISTINCT T.event_object_table FROM INFORMATION_SCHEMA.TRIGGERS T');
        $arrayTablas = [];

        foreach ($tablas as $key => $value) {
            $arrayTablas[$value->event_object_table]=$value->event_object_table;
        }
        return $arrayTablas;
    }
    
} 