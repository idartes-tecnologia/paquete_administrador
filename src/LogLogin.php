<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;
 

class LogLogin extends Model
{
    
    protected $table = 'tbl_log_login';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['i_fk_id_usuario','vc_ip','vc_dispositivo','vc_mac','vc_url','i_fk_id_modulo'];
    public $timestamps = false;
    public function usuario()
	{
		return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_usuario','id');
	}
	public function modulo(){
		return $this->belongsTo(config('usuarios.modelo_modulo'), 'i_fk_id_modulo','i_pk_id');
	}

}